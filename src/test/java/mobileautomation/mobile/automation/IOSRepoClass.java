package mobileautomation.mobile.automation;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class IOSRepoClass {

	@FindBy(xpath = "//XCUIElementTypeCell")
	public List<WebElement> lisItems;
	
	@FindBy(xpath = "//XCUIElementTypeStaticText[@name='Alert Views']")
	public WebElement alertViewsLink;
	
	@FindBy(xpath = "//XCUIElementTypeStaticText[@name='Simple']")
	public WebElement simpleLink;
	
	@FindBy(xpath = "//XCUIElementTypeStaticText[@name='A Short Title Is Best']")
	public WebElement popUpText;
	
	@FindBy(xpath = "//XCUIElementTypeButton[@name='OK']")
	public WebElement okButton;
	
}

