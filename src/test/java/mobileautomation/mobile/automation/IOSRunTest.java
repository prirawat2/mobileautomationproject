package mobileautomation.mobile.automation;

import java.io.File;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class IOSRunTest {

public static WebDriver driver;
	
	File file = new File("/Users/prirawat2/Library/Developer/Xcode/DerivedData/UIKitCatalog-ayvopjrbqfpabgbvqwnixwxfkgcn/Build/Products/Debug-iphonesimulator/UIKitCatalog.app");
	
	@BeforeMethod
	public void setUp() throws Exception {

		//Set the Desired Capabilities
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "iPhone 11");
        capabilities.setCapability("platformVersion", "13.5");
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability(MobileCapabilityType.APP, file.getAbsolutePath());
        
        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new IOSDriver(url, capabilities);
        System.out.println("Started Application -  UI Catalog");
	}
	
	@Test
	public void IOSTestCase() {
		
		IOSRepoClass repo = new IOSRepoClass();
		PageFactory.initElements(driver, repo);
		
		//To identify total number of elements
		int size = repo.lisItems.size();
		System.out.println("Total number of Elements are : "+ size);
		
		//Click on Alert Views
		repo.alertViewsLink.click();
		System.out.println("Clicked on Alert Views");
		
		//Click on Simple
		repo.simpleLink.click();
		System.out.println("Clicked on Simple");
		
		//Pop up Opens up
		String text = "A Short Title Is Best";
		if(repo.popUpText.getText().equals(text)) {
			System.out.println("The pop up appears");
			repo.okButton.click();
			System.out.println("Click on OK button");
		} else
			System.out.println("The pop up did not appear");
	}
	
	@AfterMethod
	public void quitDriver() {
		driver.quit();
	}
}
