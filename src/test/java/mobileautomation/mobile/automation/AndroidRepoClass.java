package mobileautomation.mobile.automation;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AndroidRepoClass {

	@FindBy(xpath = "//android.widget.TextView[@content-desc='Content']")
	public WebElement contentLink;
	
	@FindBy(xpath = "//android.widget.TextView[@content-desc='Assets']")
	public WebElement assetsLink;
	
	@FindBy(xpath = "//android.widget.TextView[@content-desc='Read Asset']")
	public WebElement readAssetLink;

	@FindBy(id = "io.appium.android.apis:id/text")
	public WebElement readAssetText;
	
}
