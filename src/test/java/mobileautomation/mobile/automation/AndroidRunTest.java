package mobileautomation.mobile.automation;

import java.io.File;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class AndroidRunTest {
	public static WebDriver driver;
	
	File file = new File("/Users/prirawat2/Documents/Automation-workspace/mobile.automation/src/test/java/MobileAppData/ApiDemos-debug.apk");
	
	@BeforeMethod
	public void setUp() throws Exception {

		//Set the Desired Capabilities
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "Android Emulator");
        capabilities.setCapability("platformVersion", "10");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        capabilities.setCapability(MobileCapabilityType.APP, file.getAbsolutePath());
        
        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AndroidDriver(url, capabilities);
        System.out.println("Started Application -  API Demos");
	}
	
	@Test
	public void AndroidTestCase() throws Exception {
		AndroidRepoClass repo = new AndroidRepoClass();
		PageFactory.initElements(driver, repo); 
		
		//Click on Content 
		repo.contentLink.click();
		System.out.println("Clicked on Content Link");
        
        //Click on Assets
        repo.assetsLink.click();
        System.out.println("Clicked on Assets Link");
        
        //Click on Read Asset
        repo.readAssetLink.click();
        System.out.println("Click on Read Asset Link");
        
        //Read the text and print it
        String text = repo.readAssetText.getText();
        System.out.println("The text is : "+ text);
	}
	
	@AfterMethod
	public void quitDriver() {
		driver.quit();
	}
	
}
